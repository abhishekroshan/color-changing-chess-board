let board = document.querySelector(".container");

// console.log(board);

for (let i = 1; i <= 8; i++) {
  const cellContainer = document.createElement("div");
  cellContainer.className = "cellContainer";

  for (let j = 1; j <= 8; j++) {
    const cell = document.createElement("div");
    cell.className = "cell";

    cell.dataset.x = i;
    cell.dataset.y = j;

    if ((i + j) % 2 === 0) {
      cell.style.backgroundColor = "white";
    } else {
      cell.style.backgroundColor = "black";
    }

    cellContainer.append(cell);
  }
  board.append(cellContainer);
}

//

//-------------------------------------------------------------

let redArray = [];
let initial;

//-------------------------------------------------------------

function changeColor(e) {
  setInitial(redArray, initial);

  const targetCell = e.target;

  let targetX = parseInt(targetCell.dataset.x);
  let targetY = parseInt(targetCell.dataset.y);

  let targetXPlus = targetX;
  let targetYPlus = targetY;

  let color = targetCell.style.backgroundColor;

  // console.log(color);

  // let initial = color;

  initial = e.target.style.backgroundColor;

  // console.log(initial);

  while (targetXPlus >= 1 && targetYPlus >= 1) {
    const cell = board.querySelector(
      `[data-x='${targetXPlus}'][data-y='${targetYPlus}']`
    );
    cell.style.backgroundColor = "red";

    redArray.push([targetXPlus, targetYPlus]);

    targetXPlus--;
    targetYPlus--;
  }

  let targetXMinus = targetX;
  let targetYMinus = targetY;

  while (targetXMinus <= 8 && targetYMinus <= 8) {
    const cell = board.querySelector(
      `[data-x='${targetXMinus}'][data-y='${targetYMinus}']`
    );
    cell.style.backgroundColor = "red";

    redArray.push([targetXMinus, targetYMinus]);

    targetXMinus++;
    targetYMinus++;
  }

  let xTarget = targetX;
  let yTarget = targetY;

  while (xTarget <= 8 && yTarget >= 1) {
    const cell = board.querySelector(
      `[data-x='${xTarget}'][data-y='${yTarget}']`
    );
    cell.style.backgroundColor = "red";

    redArray.push([xTarget, yTarget]);

    xTarget++;
    yTarget--;
  }

  let xxTarget = targetX;
  let yyTarget = targetY;

  while (xxTarget >= 1 && yyTarget <= 8) {
    const cell = board.querySelector(
      `[data-x='${xxTarget}'][data-y='${yyTarget}']`
    );
    cell.style.backgroundColor = "red";

    redArray.push([xxTarget, yyTarget]);

    xxTarget--;
    yyTarget++;
  }

  // console.log(redArray);
}

//

function setInitial(array, initialColor) {
  array.forEach((element) => {
    const oldCell = board.querySelector(
      `[data-x='${element[0]}'][data-y='${element[1]}']`
    );

    oldCell.style.backgroundColor = initialColor;
  });

  redArray.length = 0;
}

board.addEventListener("click", changeColor);
